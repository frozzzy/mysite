import dockerLogo from './assets/docker.svg'
import gitlabLogo from './assets/gitlab.svg'
import './App.css'

const version = import.meta.env.VITE_APP_VERSION

function App() {
  
  return (
    <>
      <div>
        <a href="https://hub.docker.com/repository/docker/frozzych/mysite/general/" target="_blank">
          <img src={dockerLogo} className="logo" alt="Docker logo" />
        </a>
        <a href="https://gitlab.com/frozzzy/mysite/" target="_blank">
          <img src={gitlabLogo} className="logo react" alt="GitLab logo" />
        </a>
      </div>
      <h1>DevOps Engineer</h1>
      <h6>by Dmitry Galyshev</h6>
      <p className="read-the-docs">
        Версия: {version ?? 'Не определена'}
      </p>
    </>
  )
}

export default App
