FROM node:14.20.1 as builder

ARG version
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN echo "VITE_APP_VERSION="$version > .env
RUN npm run build

FROM amd64/nginx:latest

COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /app/build /usr/share/nginx/html

EXPOSE 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]